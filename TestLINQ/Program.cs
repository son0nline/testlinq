﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestLINQ
{
    class Program
    {
        static void Main(string[] args)
        {

            flag:
            for(int i = 0; i < 10; i++)
            {
                for(int j = 0; j < 10; j++)
                {
                    if(i == 5)
                    {
                        break;
                    }
                    Console.Write(j + " ");
                }
                Console.WriteLine(i + " ");
            }


            string a = System.IO.Path.GetFileName(@"\TNVSVC\App_Code\OM\C_CsMaster.asmx.cs");

            Console.WriteLine(a);

            string[] listStr = new string[] { "a1", "a2", "b", "c", "d" };

            var s = from rs in listStr
                    where rs.Substring(0, 1) == "a"
                    select rs;

            string[] ar = s.ToArray();

            foreach(var item in ar)
            {
                Console.WriteLine(item);
            }

            string[] listStr2 = new string[] { "1A", "29C", "2A", "D", "C" };
            Array.Sort(listStr2);
            foreach(var item in listStr2)
            {
                Console.WriteLine(item);
            }

            List<CartLine> Lines = new List<CartLine>();
            Lines.Add(new CartLine() { ProductCode = "p1", Name = "Product1", Price = 5 });
            Lines.Add(new CartLine() { ProductCode = "p1", Name = "Product1", Price = 6 });
            Lines.Add(new CartLine() { ProductCode = "p2", Name = "Product2", Price = 12 });

            List<CartLine> result = Lines
            .GroupBy(l => l.ProductCode)
            .Select(cl => new CartLine
            {
                ProductCode = cl.First().ProductCode,
                Name = cl.First().Name,
                Price = cl.Sum(c => c.Price)
            }).ToList();

            foreach(var item in result)
            {
                Console.WriteLine(item);
            }



            List<string> dList = new List<string>();

            dList.Add("1999");
            dList.Add("1998");
            dList.Add("1997");
            dList.Add("1996");
            dList.Add("3.145");
            dList.Add("3.14");



            var lss = dList.OrderBy(o => o);

            foreach (var ls in lss)
            {
                //Console.WriteLine(ls);	
            }



            DateList al = new DateList();
            al.Add(new TargetDay()
            {
                IsLastYear = true,
                YearMonthDay = "1999"
            });


            al.Add(new TargetDay()
            {
                IsLastYear = true,
                YearMonthDay = "1998"
            });


            al.Add(new TargetDay()
            {
                IsLastYear = true,
                YearMonthDay = "1997"
            });

            al.Add(new TargetDay()
            {
                IsLastYear = true,
                YearMonthDay = "3.14"
            });

            var lsorder = al.OrderByDescending(o => o.YearMonthDay);

            foreach (var a in lsorder)
            {
                Console.WriteLine(a);

            }

            Console.WriteLine("Hello World");


            IEnumerable<string> strings = new List<string> { "first", "then", "and then", "finally" };
            // Will contain { "finally", "and then", "then", "first" }
            IEnumerable<string> results = strings.Reverse();

            foreach (var lsstr in results)
            {
                Console.WriteLine(lsstr);
            }

            Console.ReadLine();
        }
    }

    class CartLine
    {
        public string ProductCode
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public int Price
        {
            get; set;
        }

        public override string ToString()
        {
            return string.Format("Code:{0} Name:{1} Price:{2}", this.ProductCode, this.Name, this.Price);
        }
    }


    class DateList : List<TargetDay>
    {

    }

    class TargetDay
    {
        public bool IsLastYear { set; get; }

        public string YearMonthDay { set; get; }

        public override string ToString()
        {
            return YearMonthDay;
        }
    }
}
